import copy
import itertools
import os
import sys
import typing

sys.path.append(os.path.join(os.path.dirname(__file__), '..'))

from pandas import Series
from typing.io import TextIO

from coverage import get_cover_objects
from data.ulugbek.ulugbek import get_object_set
from data.objects import ObjectSet
from metrics.juravlev import dist_object_set as dist_func
from sphere import find_all_spheres, get_sphere_connected_objects, find_sphere
from utils import normalization_minmax


class BijMatrix:
    def __init__(self, object_set: ObjectSet, center_object: Series):
        self.object_set = object_set
        self.center_object = center_object
        self.object_distances = None
        self.value = None
        self.nearest_enemy_index = None  # nearest enemy index
        self.maxij = None
        self.argmaxij = None
        self.str_object_distances = ""
        self.str_class_difference = ""
        self.str_class_sequence = ""
        self.relatives_count = 0

        self._find_bij_value()

    def _find_bij_value(self):
        # Finding distances from current object (center_object)
        self.object_distances = self.object_set.objects.apply(
            lambda r: dist_func(r, self.center_object, self.object_set.features), axis=1)
        # Sorting distances
        self.object_distances.sort_values(inplace=True)
        # Getting count of first class objects
        first_class_objects_count = self.object_set.first_class_objects_count
        # Finding max radius which gathering neighbours
        f_c = 0  # first class objects count
        s_c = 0  # second class objects count

        f_count = 0
        for k in range(first_class_objects_count):
            if self.object_set.objects[self.object_set.class_feature_index][self.object_distances.index[k]] \
                    == self.object_set.class_value:
                f_count += 1

        for k in range(f_count):
            if self.object_set.objects[self.object_set.class_feature_index][self.object_distances.index[k]] \
                    == self.object_set.class_value:
                f_c += 1
                self.str_class_sequence += "1\t"
            else:
                s_c += 1
                self.str_class_sequence += "0\t"

                if self.nearest_enemy_index is None:
                    self.nearest_enemy_index = k  # because in formula (k-1)

            if self.nearest_enemy_index is None:
                self.nearest_enemy_index = f_count-1  # because in formula (k-1)

            self.str_class_difference += f"={f_c}-{s_c}\t"
            self.str_object_distances += f"{self.object_distances.values[k]:.6f}({self.object_distances.index[k]})\t"

            # If two different class objects distance is zero then no need continue
            if s_c > 0 and self.object_distances.values[k] == 0:
                break

            # If distance are equal we include all together
            if self.object_distances.values[k] == self.object_distances.values[k + 1]:
                continue

            diff = f_c - s_c
            if diff > 0 and (self.maxij is None or self.maxij < diff):
                self.maxij = diff
                self.argmaxij = k + 1

        # If nearest enemy radius equals to relative radius then remove that relative
        for ind in range(1, self.nearest_enemy_index):
            if self.object_distances.values[self.nearest_enemy_index - ind] != \
                    self.object_distances.values[self.nearest_enemy_index]:
                self.relatives_count = self.nearest_enemy_index - ind + 1
                break

        if self.relatives_count > 0 and self.maxij is not None and \
                self.object_distances.values[self.nearest_enemy_index] != 0:
            self.value = self.relatives_count * self.maxij / self.argmaxij
        else:
            self.value = 0

    def __repr__(self):
        return f"b{self.object_set.features.str_get_active_indexes()} = {self.value}, " \
               f"max={self.maxij}, " \
               f"arg max={self.argmaxij}, " \
               f"nearest_enemy_index={self.nearest_enemy_index}\n" \
               f"{self.str_class_sequence}\n" \
               f"{self.str_class_difference}\n" \
               f"{self.str_object_distances}\n"

    def __str__(self):
        return self.__repr__()


def find_all_pair_bij(object_set: ObjectSet, center_object: Series):
    result = list()

    indexes = object_set.features.get_active_indexes()

    for i, j in itertools.combinations(indexes, 2):
        obj_set = copy.deepcopy(object_set)

        for k, v in obj_set.features.items():
            v.is_active = k == i or k == j

        result.append(BijMatrix(obj_set, center_object))

    return result


def find_max_bij(bij: typing.List[BijMatrix], data_file: TextIO) -> BijMatrix:
    max_bij = None

    for b in bij:
        print(f"Object{b.center_object.name}{b.object_set.features.str_get_active_indexes()}=======")
        data_file.write("\n========================\n\n")
        data_file.write(f"{b}\n")
        if b.value > 0:
            if max_bij is None or max_bij.value < b.value:
                max_bij = b
    if max_bij:
        data_file.write(
            f"\nMax Found=====\n\n{max_bij.object_set.features.get_active_indexes()}\nEnd Max Found=====\n\n"
        )

    return max_bij


def find_row_bij_comb(object_set: ObjectSet, row: Series):
    o_folder = os.path.join(
        os.path.dirname(os.path.abspath(__file__)), 'out finding best feature')
    with open(os.path.join(o_folder, f'bij for object{row.name}.txt'),
              'w') as data_file:

        ft_indexes = object_set.features.get_active_indexes()

        for ft_num in range(2, len(ft_indexes)):
            for ft_set in itertools.combinations(ft_indexes, ft_num):
                print(f"Finding Bij for {sorted(ft_set)}")

                obj_set = copy.deepcopy(object_set)

                for k in obj_set.features.keys():
                    obj_set.features[k].is_active = k in ft_set
                b = BijMatrix(obj_set, row)

                if b.value > 0:
                    data_file.write("\n========================\n\n")
                    data_file.write(f"{b}\n")


def main(object_set: ObjectSet, should_normalize: bool):
    o_folder = os.path.join(
        os.path.dirname(os.path.abspath(__file__)), 'out finding best feature')

    if not os.path.exists(o_folder):
        os.makedirs(o_folder)

    if should_normalize:
        object_set = normalization_minmax(object_set)
        object_set.objects.to_csv(os.path.join(o_folder, 'normalized.csv'))

    # dist = get_objects_dist(object_set, dist_func)
    # dist.to_csv(os.path.join(o_folder, 'distance.csv'))
    first_class_objects_index = object_set.first_class_objects.index.values
    for index in first_class_objects_index:
        # if index < 180:
        #     continue
        print(f"Finding feature set of Object {index} ")
        row = object_set.objects.loc[index]
        # from threading import Thread
        # from multiprocessing.pool import ThreadPool
        # tp = ThreadPool(4)
        # tp.ma
        # tp.apply_async(func=find_row_bij_comb, {'object_set': object_set, 'row': row})

        with open(
                os.path.join(o_folder, f'bij for object{row.name}.txt'),
                'w') as data_file:
            # Finding Bij for first two features
            print("Finding Bij for first two features")
            bij = find_all_pair_bij(object_set, row)
            print("Finding max bij from firs two features list")
            max_bij: BijMatrix = find_max_bij(bij, data_file)

            sphere = find_sphere(row,
                                 max_bij.object_set.first_class_objects,
                                 max_bij.object_set.second_class_objects,
                                 max_bij.object_set.features,
                                 dist_func)
            data_file.write(sphere.__repr__())

            if max_bij.value is None:
                continue

            while len(max_bij.object_set.features.get_active_indexes()) < len(
                    object_set.features.get_active_indexes()):
                not_included_features = max_bij.object_set.features.get_inactive_indexes()
                res = list()

                for f in not_included_features:
                    obj_set = copy.deepcopy(max_bij.object_set)

                    obj_set.features[f].is_active = True

                    res.append(BijMatrix(obj_set, row))

                max_bij: BijMatrix = find_max_bij(res, data_file)
                if max_bij is None:
                    break

                print("Finding all spheres")
                spheres = find_all_spheres(max_bij.object_set, dist_func)
                s = spheres[index]
                print("Find connected_objects")
                s.connected_objects = get_sphere_connected_objects(s, spheres)
                data_file.write(f"{s}\n")
                data_file.write(
                    f"Stability: {len(s.connected_objects) / len(first_class_objects_index)}\n\n")
                if s.radius > 0:
                    coverage, enemies = get_cover_objects(spheres)
                    data_file.write(
                        f"Coverage objects: {len(coverage)} {coverage}\n")
                    data_file.write(
                        f"Enemies objects: {len(enemies)} {enemies}\n\n")
                    for en in s.enemies:
                        en_sphere = find_sphere(max_bij.object_set.objects.loc[en],
                                                max_bij.object_set.second_class_objects,
                                                max_bij.object_set.first_class_objects,
                                                max_bij.object_set.features,
                                                dist_func)

                        g = 0

                        for cov in first_class_objects_index:
                            if en in spheres[cov].enemies:
                                g += 1

                        data_file.write(
                            f"Noisy object {en}  {g} / {max_bij.object_set.first_class_objects_count} > "
                            f"{len(en_sphere.relatives)} / {max_bij.object_set.second_class_objects_count}")
                        if g / max_bij.object_set.first_class_objects_count > \
                                len(en_sphere.relatives) / max_bij.object_set.second_class_objects_count:
                            obj_set = copy.deepcopy(max_bij.object_set)
                            obj_set.objects.drop(en, inplace=True)
                            data_file.write(f"  Noisy object Deleted\n")
                            print("Finding all spheres")
                            s = find_sphere(row, obj_set.first_class_objects, obj_set.second_class_objects,
                                            obj_set.features, dist_func)
                            # print("Find connecting_objects")
                            # s.connecting_objects = get_sphere_connecting_objects(dist, s)
                            print("Find connected_objects")
                            s.connected_objects = get_sphere_connected_objects(s, spheres)
                            data_file.write(f"{s}\n")
                            data_file.write(
                                f"Stability:"
                                f" {len(s.connected_objects) / max_bij.object_set.first_class_objects_count}\n\n")
                        else:
                            data_file.write(f"  Noisy object NOT Deleted\n")


if __name__ == "__main__":
    objects = get_object_set()
    main(objects, True)
