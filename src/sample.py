import os

import numpy as np
from metrics.juravlev import dist_object_set
from data.objects import Features, Feature
from finding_first_features.find_object_set_by_sphere_by_pair import find2
from finding_features.find_by_phi import find as find_other_features

print(os.path.dirname(os.path.abspath(__file__)))
df = np.genfromtxt(os.path.join(os.path.dirname(os.path.abspath(__file__)), "data", "german", "german_01.dat"),
                   delimiter='\t', skip_header=1, skip_footer=1)
print(df)
print(df.shape)

features = Features()
continuous = {1, 4, 7, 10, 12, 15, 17}

for i in range(21):
    features[i] = Feature(i in continuous, i == 20)
class_value = 1

for r in df:
    if r[20] != 1:
        r[20] = 2

v = find2(df, features, 20, dist_object_set)
# v = features
# for i, f in features.items():
#     f.is_active = i == 4 or i == 20
print(v)
v2 = find_other_features(df, v, class_value, 20)
print(v2)
