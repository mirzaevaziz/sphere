import os

import pandas as pd

from data.objects import Feature, ObjectSet, Features


def get_object_set():
    df = pd.read_csv(os.path.join(os.path.dirname(os.path.abspath(__file__)), "australian.csv"), header=None)

    features = Features()
    info = [0, 1, 1, 0, 0, 0, 1, 0, 0, 1, 0, 0, 1, 1, 0]

    for i in range(len(info)):
        features[i] = Feature(info[i] == 1, i == 14)

    df.drop_duplicates(inplace=True)

    return ObjectSet(df, features, 1)
