import os

import pandas as pd

from data.objects import Feature, ObjectSet, Features


def get_object_set(class_value=1):
    df = pd.read_csv(os.path.join(os.path.dirname(os.path.abspath(__file__)), "wine.data"), header=None)

    features = Features()

    for i in range(14):
        features[i] = Feature(i != 0, i == 0)

    # df.drop_duplicates(inplace=True)

    return ObjectSet(df, features, class_value)


def main():
    df = pd.read_csv(os.path.join(os.path.dirname(os.path.abspath(__file__)), "german.data"), header=None)

    features = Features()
    continuous = {1, 4, 7, 10, 12, 15, 17}

    for i in range(21):
        features[i] = Feature(i in continuous, i == 20)

    with open(os.path.join(os.path.dirname(os.path.abspath(__file__)), "german_01.dat"),
              'w') as data_file:
        for ind, row in df.iterrows():
            for i, f in features.items():
                if f.is_continuous or f.is_class:
                    data_file.write(f"{row[i]}\t")
                else:
                    data_file.write(f"{row[i][-1:]}\t")
            data_file.write('\n')


if __name__ == "__main__":
    main()
