import typing

from pandas import DataFrame


class Feature:
    def __init__(self, is_continuous: bool = False, is_class: bool = False, is_active: bool = True):
        self.is_continuous = is_continuous
        self.is_class = is_class
        self.is_active = is_active

    def __str__(self):
        return f"IsActive:{self.is_active}" \
               f", IsClass:{self.is_class}" \
               f", IsContinuous:{self.is_continuous}"

    def __repr__(self):
        return self.__str__()


class Features(typing.Dict[int, Feature]):
    def __str__(self):
        result = ""
        for index, ft in self.items():
            result += f"{index}({ft.is_active})"
        return result

    def __repr__(self):
        return self.__str__()

    def get_inactive_indexes(self) -> typing.Set[int]:
        return {ind for ind, ft in self.items() if not ft.is_active and not ft.is_class}

    def get_active_indexes(self, include_class_feature: bool = False) -> typing.Set[int]:
        return {ind for ind, ft in self.items() if ft.is_active and ((not ft.is_class) or include_class_feature)}

    def str_get_active_indexes(self, include_class_feature: bool = False) -> str:
        return str(sorted(self.get_active_indexes(include_class_feature)))


class ObjectSet:
    def __init__(self, objects: DataFrame, features: Features, class_value: int):
        if len(objects) == 0:
            raise ValueError("Objects weren't given.")

        if len(features) == 0:
            raise ValueError("Features weren't given.")

        if len(objects.columns) != len(features):
            raise ValueError(
                "Length of objects columns doesn't match to features length")

        self.objects = objects
        self.features = features
        self.class_value = class_value

        # Finding class index
        self.class_feature_index = -1
        for k, v in features.items():
            if v.is_class:
                if self.class_feature_index > -1:
                    raise ValueError("Class feature was given more than once.")
                self.class_feature_index = k
        if self.class_feature_index == -1:
            raise ValueError("Class feature wasn't given.")

    @property
    def first_class_objects_count(self):
        return len(set(self.first_class_objects.index.values))

    @property
    def second_class_objects_count(self):
        return len(set(self.second_class_objects.index.values))

    @property
    def first_class_objects(self) -> DataFrame:
        return self.objects[
            self.objects.index.map(lambda i: self.objects[self.class_feature_index][i] == self.class_value)]

    @property
    def second_class_objects(self) -> DataFrame:
        return self.objects[
            self.objects.index.map(lambda i: self.objects[self.class_feature_index][i] != self.class_value)]

    def describe(self):
        return f"Objects count = {len(self.objects.index.values)}" \
               f", Class value = {self.class_value}" \
               f", Class objects = {self.first_class_objects_count}" \
               f", Non Class objects = {self.second_class_objects_count}"

    def __str__(self):
        return self.describe()

    def __repr__(self):
        return self.describe()
