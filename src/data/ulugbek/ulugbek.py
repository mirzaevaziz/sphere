import os

import pandas as pd

from data.objects import Feature, ObjectSet, Features


def get_object_set():
    df = pd.read_csv(os.path.join(os.path.dirname(
        os.path.abspath(__file__)), "MATBIO_NORM_MY.txt"), header=None)

    features = Features()

    for i in range(62):
        features[i] = Feature(i != 61, i == 61)

    df.drop_duplicates(inplace=True)

    return ObjectSet(df, features, 1)


def main():
    df = pd.read_csv(os.path.join(os.path.dirname(
        os.path.abspath(__file__)), "MATBIO_NORM_MY.txt"), header=None)

    features = Features()

    for i in range(61):
        features[i] = Feature(i != 60, i == 60)


if __name__ == "__main__":
    main()
