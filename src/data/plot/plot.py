import os

import pandas as pd

from data.objects import Feature, ObjectSet, Features


def get_object_set():
    df = pd.read_csv(os.path.join(os.path.dirname(os.path.abspath(__file__)), "plot1.csv"), header=None)

    features = Features()
    info = [1, 1, 0]

    for i in range(len(info)):
        features[i] = Feature(info[i] == 1, i == 2)

    df.drop_duplicates(inplace=True)

    return ObjectSet(df, features, 1)
