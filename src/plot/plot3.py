import math

import matplotlib.pyplot as plt


# import matplotlib
#
# matplotlib.use('Agg')


def plot_before_deleting_noisy_object():
    plt.rcParams['backend'] = 'TkAgg'

    objects1 = [(12.5, 10.5),
                (18, 16.5),
                (8.5, 11.5),
                (16, 11.5),
                (14, 4.5)]
    objects2 = [(19, 17.5),
                (13, 6.5)]

    fig, ax = plt.subplots()
    plt.yticks(fontname="Times New Roman", fontsize=15)
    plt.xticks(fontname="Times New Roman", fontsize=15)
    plt.axis([0, 25, 0, 20])
    ax.set_aspect(1)

    obj_count = 0
    for x, y in objects1:
        plt.plot(x, y, 'r^')
        obj_count += 1
        plt.text(x + 0.3, y, f"S$_{obj_count}$", fontdict={'size': 15}, fontname="Times New Roman")

    for x, y in objects2:
        plt.plot(x, y, 'cs')
        obj_count += 1
        plt.text(x + 0.3, y, f"S$_{obj_count}$", fontdict={'size': 15}, fontname="Times New Roman")

    radius = math.sqrt((objects1[3][0] - objects2[1][0]) ** 2 + (objects1[3][1] - objects2[1][1]) ** 2)
    c = plt.Circle(objects1[3],
                   radius,
                   facecolor='green', alpha=0.5)
    ax.add_artist(c)

    c = plt.Circle(objects2[1],
                   radius,
                   facecolor='yellow', alpha=0.5)
    ax.add_artist(c)

    radius = math.sqrt((objects1[1][0] - objects2[0][0]) ** 2 + (objects1[1][1] - objects2[0][1]) ** 2)
    c = plt.Circle(objects1[1],
                   radius,
                   facecolor='royalblue', alpha=0.5)
    ax.add_artist(c)

    radius = math.sqrt((objects1[2][0] - objects2[1][0]) ** 2 + (objects1[2][1] - objects2[1][1]) ** 2)
    c = plt.Circle(objects1[2],
                   radius,
                   facecolor='coral', alpha=0.5)
    ax.add_artist(c)

    print(f"Radius = {radius}")
    plt.show()
    plt.savefig('{}.png'.format('before'), dpi=600)
    plt.close()
    plt.clf()


def plot_after_deleting_noisy_object():
    # plt.rcParams['backend'] = 'TkAgg'

    objects1 = [(12.5, 10.5),
                (18, 18.5),
                (8.5, 11.5),
                (16, 11.5),
                (14, 4.5)]
    objects2 = [(5, 5),
                # (13, 6.5)
                ]

    fig, ax = plt.subplots()
    plt.yticks(fontname="Times New Roman", fontsize=15)
    plt.xticks(fontname="Times New Roman", fontsize=15)
    plt.axis([0, 25, 0, 20])
    ax.set_aspect(1)
    ax.xaxis.label.set_size(20)
    obj_count = 0
    for x, y in objects1:
        plt.plot(x, y, 'r^')
        obj_count += 1
        plt.text(x + 0.3, y, f"S$_{obj_count}$", fontname="Times New Roman", fontdict={'size': 15})

    for x, y in objects2:
        plt.plot(x, y, 'cs')
        obj_count += 1
        plt.text(x + 0.3, y, f"S$_{obj_count}$", fontname="Times New Roman", fontdict={'size': 15})

    radius = math.sqrt((objects1[3][0] - objects2[0][0]) ** 2 + (objects1[3][1] - objects2[0][1]) ** 2)
    c = plt.Circle(objects1[3],
                   radius,
                   facecolor='green', alpha=0.5)
    ax.add_artist(c)

    c = plt.Circle(objects2[0],
                   radius,
                   facecolor='yellow', alpha=0.5)
    ax.add_artist(c)

    radius = math.sqrt((objects1[2][0] - objects2[0][0]) ** 2 + (objects1[2][1] - objects2[0][1]) ** 2)
    c = plt.Circle(objects1[2],
                   radius,
                   facecolor='coral', alpha=0.5)
    ax.add_artist(c)

    print(f"Radius = {radius}")
    plt.show()
    plt.savefig('{}.png'.format('after'), dpi=600)
    plt.close()
    plt.clf()


def new_crit_plot():
    # plt.rcParams['backend'] = 'TkAgg'

    objects1 = [(12.5, 10.5),
                (13, 13.5),
                (8.5, 14.5),
                (16, 12.5),
                (14, 9)]
    objects2 = [(5, 5),
                (13, 12),
                (5.5, 6.5),
                (4, 3.4),
                (3, 6),
                ]

    fig, ax = plt.subplots()
    plt.axis([0, 25, 0, 20])
    ax.set_aspect(1)

    obj_count = 0
    for x, y in objects1:
        plt.plot(x, y, 'r^')
        obj_count += 1
        plt.text(x + 0.3, y, f"S$_{obj_count}$", fontdict={'size': 15})

    for x, y in objects2:
        plt.plot(x, y, 'cs')
        obj_count += 1
        plt.text(x + 0.3, y, f"S$_{{{obj_count}}}$", fontdict={'size': 15})

    # radius = math.sqrt((objects1[3][0] - objects2[0][0]) ** 2 + (objects1[3][1] - objects2[0][1]) ** 2)
    # c = plt.Circle(objects1[3],
    #                radius,
    #                facecolor='green', alpha=0.5)
    # ax.add_artist(c)
    #
    # c = plt.Circle(objects2[0],
    #                radius,
    #                facecolor='yellow', alpha=0.5)
    # ax.add_artist(c)
    #
    # radius = math.sqrt((objects1[2][0] - objects2[0][0]) ** 2 + (objects1[2][1] - objects2[0][1]) ** 2)
    # c = plt.Circle(objects1[2],
    #                radius,
    #                facecolor='coral', alpha=0.5)
    # ax.add_artist(c)

    # print(f"Radius = {radius}")
    plt.show()
    plt.savefig('{}.png'.format('after'), dpi=600)
    plt.close()
    plt.clf()


if __name__ == '__main__':
    # plot_after_deleting_noisy_object()
    # plot_before_deleting_noisy_object()
    new_crit_plot()
