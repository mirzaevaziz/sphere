import matplotlib.pyplot as plt

import pandas as pd

fig, ax = plt.subplots()
plt.axis([0, 20, 0, 20])

plt.plot([10], [10], 'r^')

## generating new objects
# import csv
#
# with open("data/plot/plot1.csv", "w") as f:
#     writer = csv.writer(f, delimiter=',')
#     writer.writerow([10, 10, 1])
#
#     for i in range(1, 40):
#         x = 0
#         y = 0
#         while True:
#             x = random.randint(-5, 5) * (random.random())
#             y = random.randint(-5, 5) * (random.random())
#             if x * x + y * y <= 9:
#                 break
#         plt.plot(10 + x, 10 + y, 'r^')
#         writer.writerow([10 + x, 10 + y, 1])
#
#     for i in range(1, 100):
#         x = 0
#         y = 0
#         while True:
#             x = random.randint(-15, 15) * random.random()
#             y = random.randint(-15, 15) * random.random()
#             if 50 > x * x + y * y > 16:
#                 break
#         plt.plot(10 + x, 10 + y, 'sb')
#         writer.writerow([10 + x, 10 + y, 2])
#
# # plt.plot([1, 2, 3, 4], [1, 4, 9, 16], 'sb')
#
# # point = (1.0, 1.0)
# # c = plt.Circle(point, 1, facecolor='green', edgecolor='orange', linewidth=15.0, alpha=0.5)
# # ax.add_artist(c)
#
# plt.show()

df = pd.read_csv('data/plot/plot1.csv', header=None)
df.drop_duplicates(inplace=True)
print(df)

for index, row in df.iterrows():
    if index in {0, 1, 2, 3, 5, 6, 10, 12, 15, 16, 17, 19, 21, 22, 24, 25, 26, 28, 29, 35, 38, 39}:
        plt.plot(row[0], row[1], 'm^')
    elif row[2] == 1 and index not in {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 15, 16, 17, 18, 19, 20, 21, 22, 23,
                                       24, 25, 26, 27, 28, 29, 30, 31, 32, 34, 35, 36, 37, 38, 39}:
        plt.plot(row[0], row[1], 'k^')
    elif row[2] == 1:
        plt.plot(row[0], row[1], 'r^')
    elif index == 46:
        plt.plot(row[0], row[1], 'cs')
    else:
        plt.plot(row[0], row[1], 'bs')

c = plt.Circle((10, 10), 4.025916953392956, facecolor='green', alpha=0.5)
ax.add_artist(c)

c = plt.Circle((df.at[46, 0], df.at[46, 1]), 4.025916953392956, facecolor='yellow', alpha=0.5)
ax.add_artist(c)

plt.show()
