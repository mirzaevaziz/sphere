import copy
import typing

import pandas as pd

import data.german.german as wn
import finding_first_features.find_object_set_by_sphere_by_pair as fff
import interval_criterions.first_criterion as first_criterion
from data.objects import ObjectSet, Features
from metrics.juravlev import dist_object_set
from utils import normalization_minmax, get_object_dist_df


def main(obj_set: ObjectSet, should_normalize: bool,
         dist_func: typing.Callable[[pd.Series, pd.Series, Features], float],
         first_feature_finder: typing.Callable[
             [pd.DataFrame, pd.DataFrame, Features,
              typing.Callable[[pd.Series, pd.Series, Features], float]], Features]):
    if should_normalize:
        obj_set = normalization_minmax(obj_set)
        print(obj_set.objects)

    # Unique non class ID
    obj_set.objects.loc[obj_set.objects[
                            obj_set.class_feature_index] != obj_set.class_value, obj_set.class_feature_index] = obj_set.class_value + 1

    first_class_objects = obj_set.first_class_objects
    second_class_objects = obj_set.second_class_objects
    first_class_objects_count = obj_set.first_class_objects_count
    second_class_objects_count = obj_set.second_class_objects_count

    # Finding first feature(s)
    features = first_feature_finder(first_class_objects, second_class_objects, obj_set.features, dist_func)

    # # TODO remove after testing German
    # features = copy.deepcopy(obj_set.features)
    # for i, ft in features.items():
    #     ft.is_active = ft.is_class or i == 4

    print(features)

    max_obj_phi = None
    max_fi = None
    max_value = None
    prev_obj_phi = None
    while True:
        if prev_obj_phi is None:
            prev_obj_phi = dict()
            for obj_ind, obj in obj_set.objects.iterrows():
                # find all distance from current object
                dist = get_object_dist_df(obj_set.objects, obj, features, obj_set.class_feature_index, dist_func)

                # Checking object's class
                if obj[obj_set.class_feature_index] == obj_set.class_value:
                    # find first criterion value
                    criterion_value = first_criterion.find(first_class_objects_count, second_class_objects_count,
                                                           obj_set.class_value, dist)

                    # find how many first class objects in [c1,c2] interval
                    first_class_count_in_interval = len(
                        criterion_value.distances[(criterion_value.distances[0] < criterion_value.distance) & (
                                criterion_value.distances[1] == obj_set.class_value)])
                    # find how many second class objects in [c1,c2] interval
                    second_class_count_in_interval = len(
                        criterion_value.distances[(criterion_value.distances[0] < criterion_value.distance) & (
                                criterion_value.distances[1] != obj_set.class_value)])

                    o1 = first_class_count_in_interval / first_class_objects_count
                    o2 = second_class_count_in_interval / second_class_objects_count
                    phi = o1 * (1 - o2)
                    prev_obj_phi[obj_ind] = phi
                else:
                    # find first criterion value
                    criterion_value = first_criterion.find(second_class_objects_count, first_class_objects_count,
                                                           obj_set.class_value + 1, dist)

                    # find how many first class objects in [c1,c2] interval
                    first_class_count_in_interval = len(
                        criterion_value.distances[(criterion_value.distances[0] < criterion_value.distance) & (
                                criterion_value.distances[1] == obj_set.class_value)])
                    # find how many second class objects in [c1,c2] interval
                    second_class_count_in_interval = len(
                        criterion_value.distances[(criterion_value.distances[0] < criterion_value.distance) & (
                                criterion_value.distances[1] != obj_set.class_value)])

                    o1 = first_class_count_in_interval / first_class_objects_count
                    o2 = second_class_count_in_interval / second_class_objects_count
                    phi = o2 * (1 - o1)
                    prev_obj_phi[obj_ind] = phi

        print(prev_obj_phi)
        # Iterating all inactive features to add one by one
        for fi, ft in features.items():
            # Miss class and already activated feature
            if ft.is_class or ft.is_active:
                continue
            # Activating feature
            ft.is_active = True
            print(f"Finding criterion value for feature {fi}")
            R = 0
            obj_phi = dict()
            # iterating by objects

            for obj_ind, obj in obj_set.objects.iterrows():
                # find all distance from current object
                dist = get_object_dist_df(obj_set.objects, obj, features, obj_set.class_feature_index, dist_func)

                # Checking object's class
                if obj[obj_set.class_feature_index] == obj_set.class_value:
                    # find first criterion value
                    criterion_value = first_criterion.find(first_class_objects_count, second_class_objects_count,
                                                           obj_set.class_value, dist)

                    # find how many first class objects in [c1,c2] interval
                    first_class_count_in_interval = len(
                        criterion_value.distances[(criterion_value.distances[0] < criterion_value.distance) & (
                                criterion_value.distances[1] == obj_set.class_value)])
                    # find how many second class objects in [c1,c2] interval
                    second_class_count_in_interval = len(
                        criterion_value.distances[(criterion_value.distances[0] < criterion_value.distance) & (
                                criterion_value.distances[1] != obj_set.class_value)])

                    o1 = first_class_count_in_interval / first_class_objects_count
                    o2 = second_class_count_in_interval / second_class_objects_count
                    phi = o1 * (1 - o2)
                    obj_phi[obj_ind] = phi
                else:
                    # find first criterion value
                    criterion_value = first_criterion.find(second_class_objects_count, first_class_objects_count,
                                                           obj_set.class_value + 1, dist)

                    # find how many first class objects in [c1,c2] interval
                    first_class_count_in_interval = len(
                        criterion_value.distances[(criterion_value.distances[0] < criterion_value.distance) & (
                                criterion_value.distances[1] == obj_set.class_value)])
                    # find how many second class objects in [c1,c2] interval
                    second_class_count_in_interval = len(
                        criterion_value.distances[(criterion_value.distances[0] < criterion_value.distance) & (
                                criterion_value.distances[1] != obj_set.class_value)])

                    o1 = first_class_count_in_interval / first_class_objects_count
                    o2 = second_class_count_in_interval / second_class_objects_count
                    phi = o2 * (1 - o1)
                    obj_phi[obj_ind] = phi

                if obj_ind not in prev_obj_phi.keys() or obj_phi[obj_ind] >= prev_obj_phi[obj_ind]:
                    R = R + 1
            R = R / (first_class_objects_count + second_class_objects_count)
            print(f"\t\tfeature {fi} = {R}, max = {max_value}")
            # Criterion for adding feature
            if R > 0.5:
                # Finding feature which gives max value of R
                if max_value is None or max_value < R:
                    max_value = R
                    max_fi = fi
                    max_obj_phi = obj_phi
                #     TODO: NEED TO CONFIRM
                # If R is equal with old value then include this feature with old feature
                # elif max_value == R:
                #     max_features[fi].is_active = True
            # Deactivate feature
            ft.is_active = False
        if max_fi is None or features[max_fi].is_active:
            break
        features[max_fi].is_active = True
        prev_obj_phi = max_obj_phi
        print(f"Found feature {max_fi} with R={max_value}")

    print(f"Found features {features}")


if __name__ == '__main__':
    main(wn.get_object_set(1), True, dist_object_set, fff.find)

    # choices = {
    #     0: 'Exit',
    #     1: {'data_src': gr.get_object_set(1)},
    #     2: {'data_src': gr.get_object_set(2)},
    #     3: {'data_src': wn.get_object_set(1)},
    #     4: {'data_src': wn.get_object_set(2)},
    #     5: {'data_src': wn.get_object_set(3)},
    # }
    #
    # while True:
    #     print(yaml.dump(choices, default_flow_style=False))
    #
    #     # pprint.pprint(choices)
    #     try:
    #         choice = int(input('Your choice: '))
    #         if choice == 0:
    #             break
    #         # main(**choices[choice],
    #         #      class_value=int(input('Class value (1): ') or 1),
    #         #      should_calculate_distances=bool(
    #         #          input('Should calculate all distances (False): '))
    #         #      )
    #     except Exception as ex:
    #         print(ex)
