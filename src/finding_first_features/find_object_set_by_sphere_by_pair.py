import copy
import itertools
import typing

import numpy as np
import pandas as pd

from data.objects import Features
from sphere import find_sphere
from utils import timeit


@timeit
def find(first_class_objects: pd.DataFrame, second_class_objects: pd.DataFrame, features: Features,
         dist_func: typing.Callable[[pd.Series, pd.Series, Features], float]) -> Features:
    # Getting copy of features
    ft = copy.deepcopy(features)
    for i in ft:
        if ft[i].is_class:
            continue
        # Setting false to all active feature except class feature
        ft[i].is_active = False

    max_s = None
    max_f1 = None
    max_f2 = None

    # Activate feature by pair combination
    for fi1, fi2 in itertools.combinations(ft.keys(), 2):
        f1 = ft[fi1]
        f2 = ft[fi2]

        if f1.is_class or f1.is_active or f2.is_class or f2.is_active:
            continue
        # Activate features
        f1.is_active = True
        f2.is_active = True
        print(f"Finding feature max of [{fi1}, {fi2}]")

        # Finding the count of relatives objects (current max)
        s = 0

        for obj in first_class_objects.itertuples(index=False):
            sphere = find_sphere(obj, first_class_objects, second_class_objects, ft,
                                 dist_func, False)
            s += len(sphere.relatives)

        for obj in second_class_objects.itertuples(index=False):
            sphere = find_sphere(obj, second_class_objects, first_class_objects, ft,
                                 dist_func, False)
            s += len(sphere.relatives)

        # If old max is none or old max less than current max then old max = current max
        if max_s is None or max_s < s:
            max_s = s
            max_f1 = fi1
            max_f2 = fi2
        print(f"\t\tcurrent = {s}, max = {max_s}")

        # Deactivate activated features
        f1.is_active = False
        f2.is_active = False

    if max_s is not None:
        ft[max_f1].is_active = True
        ft[max_f2].is_active = True
    return ft


@timeit
def find2(objects: np.ndarray, features: Features, class_feature_index: int,
          dist_func: typing.Callable[[pd.Series, pd.Series, Features], float]) -> Features:
    # Getting copy of features
    ft = copy.deepcopy(features)
    for i in ft:
        if ft[i].is_class:
            continue
        # Setting false to all active feature except class feature
        ft[i].is_active = False

    max_s = None
    max_f1 = None
    max_f2 = None

    # Activate feature by pair combination
    for fi1, fi2 in itertools.combinations(ft.keys(), 2):
        f1 = ft[fi1]
        f2 = ft[fi2]

        if f1.is_class or f1.is_active or f2.is_class or f2.is_active:
            continue
        # Activate features
        f1.is_active = True
        f2.is_active = True
        print(f"Finding feature max of [{fi1}, {fi2}]")

        # Finding the count of relatives objects (current max)
        s = 0

        obj_count = range(objects.shape[0])
        dist = np.zeros(objects.shape[0])
        for i in obj_count:
            obj = objects[i]
            radius = None
            for j in obj_count:
                obj2 = objects[j]

                # dist[j] = dist_func(obj, obj2, ft)
                dist[j] = 0.0

                if f1.is_continuous:
                    dist[j] += abs(obj[fi1] - obj2[fi1])
                elif obj[fi1] != obj2[fi1]:
                    dist[j] += 1

                if f2.is_continuous:
                    dist[j] += abs(obj[fi2] - obj2[fi2])
                elif obj[fi2] != obj2[fi2]:
                    dist[j] += 1

                if obj[class_feature_index] != obj2[class_feature_index] and (radius is None or radius > dist[j]):
                    radius = dist[j]

            rel_count = 0
            for j in obj_count:
                d = dist[j]
                if d < radius:
                    rel_count += 1

            s += rel_count

        # If old max is none or old max less than current max then old max = current max
        if max_s is None or max_s < s:
            max_s = s
            max_f1 = fi1
            max_f2 = fi2
        print(f"\t\tcurrent = {s}, max = {max_s}")

        # Deactivate activated features
        f1.is_active = False
        f2.is_active = False

    if max_s is not None:
        ft[max_f1].is_active = True
        ft[max_f2].is_active = True
    return ft


