import copy
import typing

import numpy as np
import pandas as pd

from data.objects import Features
from sphere import find_sphere
from utils import timeit


def find(first_class_objects: pd.DataFrame, second_class_objects: pd.DataFrame, features: Features,
         dist_func: typing.Callable[[pd.Series, pd.Series, Features], float]) -> Features:
    # Getting copy of features
    ft = copy.deepcopy(features)
    for i in ft:
        if ft[i].is_class:
            continue
        # Setting false to all active feature except class feature
        ft[i].is_active = False

    max_s = None
    max_ft = None
    # Activate feature one by one
    for i, f in ft.items():
        if f.is_class or f.is_active:
            continue
        # Activate feature
        f.is_active = True

        # Finding the count of relatives objects (current max)
        s = 0

        for obj_ind in first_class_objects.index.values:
            sphere = find_sphere(first_class_objects.loc[obj_ind], first_class_objects, second_class_objects, ft,
                                 dist_func)
            s += len(sphere.relatives)

        for obj_ind in second_class_objects.index.values:
            sphere = find_sphere(second_class_objects.loc[obj_ind], second_class_objects, first_class_objects, ft,
                                 dist_func)
            s += len(sphere.relatives)

        # If old max is none or old max less than current max then old max = current max
        if max_s is None or max_s < s:
            max_s = s
            max_ft = i

        # Deactivate activated feature
        f.is_active = False

    if max_ft is not None:
        ft[max_ft].is_active = True
    return ft


@timeit
def find2(objects: np.ndarray, features: Features, class_feature_index: int,
          dist_func: typing.Callable[[pd.Series, pd.Series, Features], float]) -> Features:
    # Getting copy of features
    ft = copy.deepcopy(features)
    for i in ft:
        if ft[i].is_class:
            continue
        # Setting false to all active feature except class feature
        ft[i].is_active = False

    max_s = None
    max_ft = None
    # Activate feature one by one
    for fi, f in ft.items():
        if f.is_class or f.is_active:
            continue
        # Activate feature
        f.is_active = True
        print(f"Finding feature max of [{fi}]")
        # Finding the count of relatives objects (current max)
        s = 0

        obj_count = range(objects.shape[0])
        dist = np.zeros(objects.shape[0])
        for i in obj_count:
            obj = objects[i]
            radius = None
            for j in obj_count:
                obj2 = objects[j]

                # dist[j] = dist_func(obj, obj2, ft)
                dist[j] = 0.0

                if f.is_continuous:
                    dist[j] += abs(obj[fi] - obj2[fi])
                elif obj[fi] != obj2[fi]:
                    dist[j] += 1

                if obj[class_feature_index] != obj2[class_feature_index] and (radius is None or radius > dist[j]):
                    radius = dist[j]

            rel_count = 0
            for j in obj_count:
                d = dist[j]
                if d < radius:
                    rel_count += 1

            s += rel_count

        # If old max is none or old max less than current max then old max = current max
        if max_s is None or max_s < s:
            max_s = s
            max_ft = fi
        print(f"\t\tcurrent = {s}, max = {max_s}")

        # Deactivate activated feature
        f.is_active = False

    if max_ft is not None:
        ft[max_ft].is_active = True
    return ft


@timeit
def find3(objects: np.ndarray, features: Features, class_feature_index: int,
          dist_func: typing.Callable[[pd.Series, pd.Series, Features], float]) -> Features:
    # Getting copy of features
    ft = copy.deepcopy(features)
    for i in ft:
        if ft[i].is_class:
            continue
        # Setting false to all active feature except class feature
        ft[i].is_active = False

    max_s = None
    max_ft = None
    # Activate feature one by one
    for fi, f in ft.items():
        if f.is_class or f.is_active:
            continue
        # Activate feature
        f.is_active = True
        print(f"Finding feature max of [{fi}]")
        # Finding the count of relatives objects (current max)
        s = 0

        obj_count = range(objects.shape[0])
        dist = np.zeros(objects.shape[0])
        for i in obj_count:
            obj = objects[i]
            radius = None
            for j in obj_count:
                obj2 = objects[j]

                dist[j] = dist_func(obj, obj2, ft)

                if obj[class_feature_index] != obj2[class_feature_index] and (radius is None or radius > dist[j]):
                    radius = dist[j]

            rel_count = 0
            for j in obj_count:
                d = dist[j]
                if d < radius:
                    rel_count += 1

            s += rel_count

        # If old max is none or old max less than current max then old max = current max
        if max_s is None or max_s < s:
            max_s = s
            max_ft = fi
        print(f"\t\tcurrent = {s}, max = {max_s}")

        # Deactivate activated feature
        f.is_active = False

    if max_ft is not None:
        ft[max_ft].is_active = True
    return ft
