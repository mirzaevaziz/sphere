import pandas as pd
import typing

from sphere import Sphere


def get_cover_objects(spheres: typing.Dict[int, Sphere]):
    """
Returns Tuple(coverage objects indexes as set, nearest enemy objects indexes as set)
    :param spheres: Dictionary of spheres which has value of coverage and enemies
    :return: Tuple of two sets of indexes (coverage, nearest_enemies)
    """
    coverage = set()
    nearest_enemies = set()
    for sphere in spheres.values():
        coverage.update(sphere.coverage)
        nearest_enemies.update(sphere.enemies)
    return coverage, nearest_enemies


def get_cover_helper_matrix(df1, df2, dist, coverage):
    """ Returns helper matrix of cover relation with 1st class objects.
        Keyword arguments:
            df1 -- 1st class objects (DataFrame)
            df2 -- other class objects (DataFrame)
            dist -- Matrix of distances between all objects
            coverage -- set of coverage objects' indexes
    """
    cover_helper = pd.DataFrame(columns=list(coverage), index=df1.index)
    # finding cover helper matrix
    print('Finding cover helper matrix')
    for index, row in cover_helper.iterrows():
        min_dist_df2 = dist[index].loc[df2.index].min()
        relatives = df1[(dist[index].get(df1.index) <
                         min_dist_df2)].index.values
        for ind in row.index.values:
            row[ind] = int(ind in relatives)
        print(f'Object # {index} contains cover object(s): ',
              list(row.index[row[row.index] == 1]))
    return cover_helper
