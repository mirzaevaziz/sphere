import csv
import os

import pandas as pd
import yaml

from coverage import get_cover_helper_matrix, get_cover_objects
from grouping import find_groups
from metrics import euclidean
from utils import get_objects_dist, normalization_minmax


def main(data_src, class_property_index, should_normalize, class_value, should_calculate_distances=True):
    if not os.path.isfile(data_src):
        print('Data file is not found!')
        return

    df = pd.read_csv(data_src, header=None)
    df.drop_duplicates(inplace=True)
    print(df)

    if class_property_index + 1 != len(df.columns):
        cols = df.columns.tolist()[class_property_index + 1:]
        cols.append(df.columns.tolist()[class_property_index])
        df = df[cols]
        class_property_index = len(df.columns) - 1
        print(df)

    if should_normalize:
        df = normalization_minmax(df, df.columns[class_property_index])
        print(df)

    df1 = df[(df[df.columns[class_property_index]] == class_value)]
    if len(df1) == 0:
        print("Couldn't find class objects.")
        return
    df2 = df[(df[df.columns[class_property_index]] != class_value)]

    o_folder = os.path.join(os.path.dirname(os.path.abspath(data_src)), 'out')
    if not os.path.exists(o_folder):
        os.makedirs(o_folder)

    # distances between objects
    if should_calculate_distances or not os.path.isfile(os.path.join(o_folder, 'distance.csv')):
        dist = get_objects_dist(df, euclidean.dist)
        dist.to_csv(os.path.join(o_folder, 'distance.csv'))
    else:
        dist = pd.read_csv(os.path.join(o_folder, 'distance.csv'))
        dist.set_index('Unnamed: 0', drop=True, inplace=True)
        dist.columns = dist.index.tolist()

    # finding coverage and nearest enemies
    coverage, nearest_enemies = get_cover_objects(df1, df2, dist)

    print('Nearest enemies: ', nearest_enemies)
    print('Nearest enemies count: ', len(nearest_enemies))
    with open(os.path.join(o_folder, 'nearest_enemies.csv'), 'w') as data_csv:
        coverage_writer = csv.writer(data_csv)
        coverage_writer.writerow(nearest_enemies)

    print('Coverage objects: ', coverage)
    print('Coverage objects count:', len(coverage))
    with open(os.path.join(o_folder, 'coverage.csv'), 'w') as data_csv:
        coverage_writer = csv.writer(data_csv)
        coverage_writer.writerow(coverage)

    # finding cover helper matrix
    cover_helper = get_cover_helper_matrix(df1, df2, dist, coverage)
    cover_helper.to_csv(os.path.join(o_folder, 'cover_helper.csv'))

    # finding groups

    groups = find_groups(cover_helper)

    all_obj = set()
    all_cov = set()

    for gr in groups:
        if len(all_cov.intersection(gr.coverage)) > 0:
            print(f'Group #{gr.number} has coverage from another group')
        if len(all_obj.intersection(gr.objects)) > 0:
            print(f'Group #{gr.number} has objects from another group')
        all_cov.update(gr.coverage)
        all_obj.update(gr.objects)

    print(len(all_cov))
    print(len(all_obj))


if __name__ == '__main__':
    choices = {
        0: 'Exit',
        1: {'data_src': './data/wine/wine.data', 'class_property_index': 0, 'should_normalize': True},
        2: {'data_src': './data/wine/wine.data', 'class_property_index': 0, 'should_normalize': False},
        3: {'data_src': './data/liver/bupa.data', 'class_property_index': 6, 'should_normalize': True},
        4: {'data_src': './data/liver/bupa.data', 'class_property_index': 6, 'should_normalize': False},
        5: {'data_src': './data/giper/giper.txt', 'class_property_index': 29, 'should_normalize': True},
        6: {'data_src': './data/giper/giper.txt', 'class_property_index': 29, 'should_normalize': False},
    }

    while True:
        print(yaml.dump(choices, default_flow_style=False))

        # pprint.pprint(choices)
        try:
            choice = int(input('Your choice: '))
            if choice == 0:
                break
            main(**choices[choice],
                 class_value=int(input('Class value (1): ') or 1),
                 should_calculate_distances=bool(
                     input('Should calculate all distances (False): '))
                 )
        except Exception as ex:
            print(ex)
