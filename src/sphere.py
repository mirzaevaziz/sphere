import typing
from typing import Dict

import pandas as pd

from utils import timeit
from data.objects import ObjectSet, Features


class Sphere:
    """
    Sphere information of an object.
    Contains:
        central object's index
        radius of sphere
        set of relatives indexes which inside sphere
        set of nearest enemies indexes
        set of connecting object indexes
        set of connected objects indexes
    """

    def __init__(self,
                 object_index: int,
                 radius: float,
                 relatives: typing.Set[int],
                 enemies: typing.Set[int],
                 coverage: typing.Set[int],
                 connecting_objects: typing.Set[int] = None,
                 connected_objects: typing.Set[int] = None):
        self.object_index = object_index
        self.radius = radius
        self.relatives = relatives
        self.enemies = enemies
        self.coverage = coverage
        self.connecting_objects = connecting_objects
        self.connected_objects = connected_objects

    def __repr__(self):
        return f"Sphere {self.object_index}: radius = {self.radius}\n" \
               f"\trelatives = {len(self.relatives)} {self.relatives}\n" \
               f"\tenemies= {len(self.enemies)} {self.enemies}\n" \
               f"\tconnecting_objects = {len(self.connecting_objects) if self.connecting_objects else 0} " \
               f"{{{sorted(self.connecting_objects) if self.connecting_objects is not None else None}}}\n" \
               f"\tconnected_objects = {len(self.connected_objects) if self.connected_objects else 0} " \
               f"{{{sorted(self.connected_objects) if self.connected_objects is not None else None}}}\n"


def find_all_spheres(object_set: ObjectSet,
                     distance_function: typing.Callable[[pd.Series, pd.Series, Features], float]):
    result: Dict[int, Sphere] = dict()

    first_class_objects = object_set.first_class_objects
    # second_class_objects = object_set.second_class_objects

    for index in first_class_objects.index.values:
        first_object = first_class_objects.loc[index]

        # result[index] = find_sphere(first_object, first_class_objects, second_class_objects, object_set.features,
        #                             distance_function)
        result[index] = find_sphere2(first_object, object_set, distance_function)
    return result


def find_sphere2(first_object: pd.Series, object_set: ObjectSet,
                 distance_function: typing.Callable[[pd.Series, pd.Series, Features], float]):
    dist = object_set.objects.apply(lambda r: distance_function(first_object, r, object_set.features),
                                    axis=1).to_frame()
    dist[1] = object_set.objects[object_set.class_feature_index]
    radius = dist[dist[1] != object_set.class_value][0].min()
    relatives = set(dist[(dist[1] == object_set.class_value) & (dist[0] < radius)].index)
    enemies = set(set(dist[(dist[1] != object_set.class_value) & (dist[0] == radius)].index))

    coverage = set()
    connecting_objects = set()

    # for enemy in enemies:
    #     _min = None
    #     enemy_object = second_class_objects.loc[enemy]
    #     to_first_class_dist = first_class_objects.apply(
    #         lambda r: distance_function(enemy_object, r, features) if r.name in relatives else None,
    #         axis=1)
    #     _min = to_first_class_dist.min()
    #     coverage.update(set(to_first_class_dist.index[to_first_class_dist == _min]))
    #     connecting_objects.update(set(to_first_class_dist.index[to_first_class_dist <= radius]))

    return Sphere(first_object.name, radius, relatives, enemies, coverage, connecting_objects)


# @timeit
def find_sphere(first_object: pd.Series,
                first_class_objects: pd.DataFrame,
                second_class_objects: pd.DataFrame,
                features: Features,
                distance_function: typing.Callable[[pd.Series, pd.Series, Features], float],
                find_coverage: bool = True):
    to_first_class_dist = first_class_objects.apply(
        lambda r: distance_function(first_object, r, features), axis=1)

    to_second_class_dist = second_class_objects.apply(
        lambda r: distance_function(first_object, r, features), axis=1)

    radius = to_second_class_dist.min()

    relatives = set(to_first_class_dist.index[to_first_class_dist < radius])

    if find_coverage:
        coverage = set()
        connecting_objects = set()
        enemies = {to_second_class_dist.idxmin(), }  # set(to_second_class_dist.index[to_second_class_dist == radius])
        for enemy in enemies:
            _min = None
            enemy_object = second_class_objects.loc[enemy]
            to_first_class_dist = first_class_objects.apply(
                lambda r: distance_function(enemy_object, r, features) if r.name in relatives else None,
                axis=1)
            _min = to_first_class_dist.min()
            coverage.update(set(to_first_class_dist.index[to_first_class_dist == _min]))
            connecting_objects.update(set(to_first_class_dist.index[to_first_class_dist <= radius]))
            return Sphere(first_object.name, radius, relatives, enemies, coverage, connecting_objects)

    return Sphere(-1, radius, relatives, None, None, None)


def get_objects_spheres(object_set: ObjectSet,
                        distances: pd.DataFrame) -> typing.Dict[int, Sphere]:
    """
Getting all spheres to given object set. Distance between objects calculated with distance function.
    :param object_set: Given object set with features.
    :param distances: Distances between all objects in object set
    :return: Dictionary of spheres
    :rtype: typing.Dict[int, Sphere]
    """

    result = _get_objects_spheres(object_set.first_class_objects,
                                  object_set.second_class_objects, distances)

    return result


def _get_objects_spheres(first_class_objects: pd.DataFrame,
                         second_class_objects: pd.DataFrame,
                         distances: pd.DataFrame) -> typing.Dict[int, Sphere]:
    """
Getting all spheres to given object set. Distance between objects calculated with distance function.
    :param first_class_objects: First class objects indexes
    :param second_class_objects: Second class objects indexes
    :param distances: DataFrame of all objects distances
    :return: Dictionary of spheres
    :rtype: typing.Dict[int, Sphere]
    """

    result: Dict[int, Sphere] = dict()

    for index in first_class_objects.index.values:
        dist = distances[index]
        radius = None

        for ind in second_class_objects.index.values:
            if radius is None or (ind in dist.index and dist[ind] < radius):
                radius = dist[ind]

        relatives = set()

        for ind in first_class_objects.index.values:
            if ind in dist.index and dist[ind] < radius:
                relatives.add(ind)

        enemies = set()

        for ind in second_class_objects.index.values:
            if ind in dist.index and dist[ind] == radius:
                enemies.add(ind)

        coverage = set()

        for enemy in enemies:
            _min = None
            dist = distances[enemy]

            for rel in relatives:
                if _min is None or _min > dist[rel]:
                    _min = dist[rel]

            for rel in relatives:
                if _min == dist[rel]:
                    coverage.add(rel)

        result[index] = Sphere(index, radius, relatives, enemies, coverage)

    return result


def get_all_enemies_from_spheres(
        spheres: typing.Dict[int, Sphere]) -> typing.Set[int]:
    """
Returns set of enemy object indexes.
    :param spheres: dictionary of object spheres
    :return: set of enemy object indexes
    """
    all_enemies = set()

    for sphere in spheres.values():
        all_enemies.update(sphere.enemies)

    return all_enemies


def get_sphere_connecting_objects(distances: pd.DataFrame, sphere: Sphere) \
        -> typing.Set[int]:
    """
Gets connecting objects indexes
    :param distances: Distances of all objects as DataFrame
    :param sphere: Current sphere with fields radius, enemies, relatives
    :return: Indexes of connecting objects
    """
    connecting_objects = set()

    for enemy_index in sphere.enemies:
        dist = distances[enemy_index]

        for ind in sphere.relatives:
            if dist[ind] <= sphere.radius:
                connecting_objects.add(ind)

    return connecting_objects


def get_sphere_connected_objects(sphere: Sphere, spheres: typing.Dict[int, Sphere]) \
        -> typing.Set[int]:
    """ Returns dictionary of set of object indexes, which near to enemy
            object in this sphere radius.
        Key is central object index, value is set of connection object indexes.
        Keyword arguments:
            spheres -- dictionary of object spheres
            dist -- Matrix of distances between all objects
    """

    result = set()

    for s in spheres.values():
        if len(s.relatives.intersection(sphere.connecting_objects)) == 0:
            continue
        result.add(s.object_index)

    return result
