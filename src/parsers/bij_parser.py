from collections import namedtuple

Bij = namedtuple('Bij', ['str', 'val', 'ft_count'])

with open('/home/ts/Projects/sphere/src/algorithms/out finding best feature/bij for object324.txt', 'r') as data_file:
    ft_len = 2
    lst = list()
    for line in data_file:
        if line[:2] == 'b[':
            bij = Bij(line, float(line[line.index('] = ') + 4:line.index(', max=')]),
                      len(line[2: line.index(']')].split(', ')))
            if ft_len != bij.ft_count:
                lst.sort(key=lambda t: t.val, reverse=True)
                with open(
                        f'/home/ts/Projects/sphere/src/algorithms/out finding best feature/bij for object324_{ft_len}.txt',
                        'w') as dt:
                    for f in lst:
                        dt.write(f"{f.str}\n")
                ft_len = bij.ft_count
                lst = list()
            lst.append(bij)
            print(line)
