from pandas import Series

from data.objects import Features


def dist_object_set(obj1: Series, obj2: Series, features: Features):
    result = 0.0
    for index, ft in features.items():
        if not ft.is_active or ft.is_class:
            continue

        if ft.is_continuous:
            result = result + abs(obj1[index] - obj2[index])
        elif obj1[index] != obj2[index]:
            result = result + 1
    return result
