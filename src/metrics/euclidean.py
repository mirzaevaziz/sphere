from math import sqrt

from pandas import Series

from data.objects import Features


def dist_object_set(obj1: Series, obj2: Series, features: Features) -> float:
    result = 0.0
    for index, ft in features.items():
        if ft.is_continuous and ft.is_active:
            result = result + (obj1[index] - obj2[index]) ** 2
    return sqrt(result)
