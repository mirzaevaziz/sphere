import os

import pandas as pd
import yaml

from metrics import euclidean
from sphere import get_objects_spheres, get_sphere_connecting_objects, get_sphere_connected_objects
from utils import get_objects_dist, normalization_minmax


def main(data_src, class_property_index, should_normalize: bool, class_value,
         should_calculate_distances=True):
    if not os.path.isfile(data_src):
        print('Data file is not found!')
        return

    df = pd.read_csv(data_src, header=None)
    df.drop_duplicates(inplace=True)
    print(df)

    if class_property_index + 1 != len(df.columns):
        cols = df.columns.tolist()[class_property_index + 1:]
        cols.append(df.columns.tolist()[class_property_index])
        df = df[cols]
        class_property_index = len(df.columns) - 1
        print(df)

    if should_normalize:
        df = normalization_minmax(df, df.columns[class_property_index])
        print(df)


    df1 = df[(df[df.columns[class_property_index]] == class_value)]
    if len(df1) == 0:
        print("Couldn't find class objects.")
        return
    df2 = df[(df[df.columns[class_property_index]] != class_value)]

    o_folder = os.path.join(os.path.dirname(os.path.abspath(data_src)), 'out')
    if not os.path.exists(o_folder):
        os.makedirs(o_folder)

    # distances between objects
    if should_calculate_distances \
            or not os.path.isfile(os.path.join(o_folder, 'distance.csv')):
        dist = get_objects_dist(df, euclidean.dist)
        dist.to_csv(os.path.join(o_folder, 'distance.csv'))
    else:
        dist = pd.read_csv(os.path.join(o_folder, 'distance.csv'))
        dist.set_index('Unnamed: 0', drop=True, inplace=True)
        dist.columns = dist.index.tolist()

    # finding all spheres
    spheres = get_objects_spheres(df1, df2, dist)
    with open(os.path.join(o_folder, 'spheres.txt'), 'w') as data_file:
        for sphere in spheres.values():
            data_file.write(f"Object #{sphere.object_index} ========================================\n")
            data_file.write(f"  Radius: {sphere.radius}\n")
            data_file.write(f"  Relatives: {sphere.relatives}  (len: {len(sphere.relatives)})\n")
            data_file.write(f"  Enemies: {sphere.enemies}  (len: {len(sphere.enemies)})\n")
            data_file.write(f"END Object #{sphere.object_index} ====================================\n")

    # find connection objects
    connection_objects = get_sphere_connecting_objects(spheres, dist)
    with open(os.path.join(o_folder, 'spheres_connection_objects.txt'), 'w') as data_file:
        for index, objects in connection_objects.items():
            data_file.write(f"Object #{index} ========================================\n")
            data_file.write(f"  Objects: {objects}  (len: {len(objects)})\n")
            data_file.write(f"END Object #{index} ====================================\n")

    # find related objects
    related_objects = get_sphere_connected_objects(spheres)
    with open(os.path.join(o_folder, 'spheres_related_objects.txt'), 'w') as data_file:
        for index, val in related_objects.items():
            data_file.write(f"Object #{index} ========================================\n")
            data_file.write(f"  Relative objects: {val}  "
                            f"(len: {len(val)})\n")
            data_file.write(f"  Compactness: {len(val) / len(df1)}\n")
            data_file.write(f"END Object #{index} ====================================\n")


if __name__ == '__main__':
    choices = {

        0: 'Exit',
        1: {'data_src': './data/wine/wine.data', 'class_property_index': 0, 'should_normalize': True},
        2: {'data_src': './data/wine/wine.data', 'class_property_index': 0, 'should_normalize': False},
        3: {'data_src': './data/liver/bupa.data', 'class_property_index': 6, 'should_normalize': True},
        4: {'data_src': './data/liver/bupa.data', 'class_property_index': 6, 'should_normalize': False},
        5: {'data_src': './data/giper/giper.txt', 'class_property_index': 29, 'should_normalize': True},
        6: {'data_src': './data/giper/giper.txt', 'class_property_index': 29, 'should_normalize': False},
        7: {'data_src': './data/plot/plot1.csv', 'class_property_index': 2, 'should_normalize': True},
        8: {'data_src': './data/australian/Australian.txt', 'class_property_index': 14, 'should_normalize': True},
    }

    while True:
        print(yaml.dump(choices, default_flow_style=False))

        # pprint.pprint(choices)
        try:
            choice = int(input('Your choice: '))
            if choice == 0:
                break
            main(**choices[choice],
                 class_value=int(input('Class value (1): ') or 1),
                 should_calculate_distances=bool(
                     input('Should calculate all distances (False): '))
                 )
        except Exception as ex:
            print(ex)
