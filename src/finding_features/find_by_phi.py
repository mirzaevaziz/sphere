import numpy as np
import itertools

import interval_criterions.first_criterion as criterion
from data.objects import Features
from utils import timeit


def find(objects: np.ndarray, features: Features, class_value: int, class_feature_index: int):
    max_obj_phi = None
    max_fi = None
    max_value = None
    prev_obj_phi = None
    obj_count = range(objects.shape[0])

    first_class_objects_count = 0
    second_class_objects_count = 0
    for i in obj_count:
        if objects[i][class_feature_index] == class_value:
            first_class_objects_count += 1
        else:
            second_class_objects_count += 1

    active_features = features.get_active_indexes()
    distances = np.zeros((objects.shape[0], objects.shape[0]))

    for i, j in itertools.combinations(obj_count, 2):
        for fi in active_features:
            if features[fi].is_continuous:
                distances[i, j] += abs(objects[i][fi] - objects[j][fi])
            elif objects[i][fi] != objects[j][fi]:
                distances[i, j] += 1
            distances[j, i] = distances[i, j]

    while True:
        if prev_obj_phi is None:
            print(f"Finding phi for features {active_features}")
            prev_obj_phi = find_phi(active_features, class_feature_index, class_value, features,
                                    objects, obj_count, first_class_objects_count, second_class_objects_count, distances)
        print(prev_obj_phi)
        # Iterating all inactive features to add one by one
        for fi, ft in features.items():
            # Miss class and already activated feature
            if ft.is_class or ft.is_active:
                continue
            # Activating feature
            ft.is_active = True
            print(f"Finding criterion value for feature {fi}")
            for i, j in itertools.combinations(obj_count, 2):
                if features[fi].is_continuous:
                    distances[i, j] += abs(objects[i][fi] - objects[j][fi])
                elif objects[i][fi] != objects[j][fi]:
                    distances[i, j] += 1
                distances[j, i] = distances[i, j]

            R = 0
            active_features.add(fi)
            obj_phi = find_phi(active_features, class_feature_index, class_value, features,
                               objects, obj_count, first_class_objects_count, second_class_objects_count, distances)
            # iterating by objects
            for obj_ind in obj_count:
                if obj_phi[obj_ind] >= prev_obj_phi[obj_ind]:
                    R = R + 1
            R = R / (first_class_objects_count + second_class_objects_count)
            print(f"\t\tfeature {fi} = {R}, max = {max_value}")
            # Criterion for adding feature
            if R > 0.5:
                # Finding feature which gives max value of R
                if max_value is None or max_value < R:
                    max_value = R
                    max_fi = fi
                    max_obj_phi = obj_phi
            # Deactivate feature
            ft.is_active = False
            active_features.remove(fi)
            for i, j in itertools.combinations(obj_count, 2):
                if features[fi].is_continuous:
                    distances[i, j] -= abs(objects[i][fi] - objects[j][fi])
                elif objects[i][fi] != objects[j][fi]:
                    distances[i, j] -= 1
                distances[j, i] = distances[i, j]
        if max_fi is None or features[max_fi].is_active:
            break
        features[max_fi].is_active = True
        active_features.add(max_fi)
        prev_obj_phi = max_obj_phi
        for i, j in itertools.combinations(obj_count, 2):
            if features[max_fi].is_continuous:
                distances[i, j] += abs(objects[i][max_fi] - objects[j][max_fi])
            elif objects[i][max_fi] != objects[j][max_fi]:
                distances[i, j] += 1
            distances[j, i] = distances[i, j]
        print(f"Found feature {max_fi} with R={max_value}")
    return features

@timeit
def find_phi(active_features, class_feature_index, class_value, features, objects, obj_count, first_class_objects_count,
             second_class_objects_count, distances):
    phi = np.zeros(len(obj_count))
    for i in obj_count:
        obj = objects[i]
        criterion_value = criterion.find2(first_class_objects_count, second_class_objects_count, class_value,
                                          class_feature_index, objects, distances[i])

        # find how many first class objects in [c1,c2] interval
        first_class_count_in_interval = 0
        # find how many second class objects in [c1,c2] interval
        second_class_count_in_interval = 0
        for j in obj_count:
            if distances[i][j] > criterion_value.distance:
                continue
            if objects[j][class_feature_index] == class_value:
                first_class_count_in_interval += 1
            else:
                second_class_count_in_interval += 1
        o1 = 0
        o2 = 0
        if objects[i][class_feature_index] == class_value:
            o1 = first_class_count_in_interval / first_class_objects_count
            o2 = second_class_count_in_interval / second_class_objects_count
        else:
            o2 = first_class_count_in_interval / first_class_objects_count
            o1 = second_class_count_in_interval / second_class_objects_count
        phi[i] = o1 * (1 - o2)
    return phi
