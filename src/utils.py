import copy
import itertools
import time
import typing

import pandas as pd

from data.objects import ObjectSet, Features


def get_object_dist(object_set: ObjectSet,
                    obj: pd.Series,
                    distance_function: typing.Callable[[pd.Series, pd.Series, Features], float]):
    # if object_set.excluded_objects:
    #     result = object_set.objects.apply(
    #         lambda x: distance_function(obj, x,
    #                                     object_set.features) if x.name not in object_set.excluded_objects else None,
    #         axis=1)
    # else:
    result = object_set.objects.apply(lambda x: distance_function(obj, x, object_set.features), axis=1)
    result = result.to_frame()
    result[1] = object_set.objects[object_set.class_feature_index]
    return result


def get_object_dist_df(object_set: pd.DataFrame,
                       obj: pd.Series,
                       features: Features,
                       class_feature_index: int,
                       distance_function: typing.Callable[[pd.Series, pd.Series, Features], float]):
    # if object_set.excluded_objects:
    #     result = object_set.objects.apply(
    #         lambda x: distance_function(obj, x,
    #                                     object_set.features) if x.name not in object_set.excluded_objects else None,
    #         axis=1)
    # else:
    result = object_set.apply(lambda x: distance_function(obj, x, features), axis=1)
    result = result.to_frame()
    result[1] = object_set[class_feature_index]
    return result


def get_objects_dist(object_set: ObjectSet,
                     distance_function: typing.Callable[[pd.Series, pd.Series, Features], float]) -> pd.DataFrame:
    """
Calculates all distances between objects
    :param object_set: Given object set with features
    :param distance_function: Callable function to calculate distance between two objects
    """

    indexes = set(object_set.objects.index.values).difference(object_set.excluded_objects)
    indexes2 = set(object_set.first_class_objects.index.values).difference(object_set.excluded_objects)

    dist = pd.DataFrame(columns=indexes, index=indexes)

    for i in indexes:
        dist[i][i] = 0

    for ind1, ind2 in itertools.product(indexes, indexes2):
        if ind1 == ind2:
            continue
        dist[ind1][ind2] = distance_function(object_set.objects.loc[ind1],
                                             object_set.objects.loc[ind2],
                                             object_set.features)
        dist[ind2][ind1] = dist[ind1][ind2]
    return dist


def normalization_minmax(object_set: ObjectSet):
    result = copy.deepcopy(object_set)

    for k, v in result.features.items():
        if not v.is_continuous:
            continue
        max_value = result.objects[k].max()
        min_value = result.objects[k].min()
        result.objects[k] = (result.objects[k] - min_value) / (max_value - min_value)
    return result


def timeit(method):
    def timed(*args, **kw):
        ts = time.time()
        result = method(*args, **kw)
        te = time.time()
        if 'log_time' in kw:
            name = kw.get('log_name', method.__name__.upper())
            kw['log_time'][name] = int((te - ts) * 1000)
        else:
            print('%r  %2.2f ms' % \
                  (method.__name__, (te - ts) * 1000))
        return result

    return timed
