class ObjectGroup:
    def __init__(self, number, coverage=set(), objects=set()):
        self.number = number
        self.coverage = coverage
        self.objects = objects


def find_groups(cover_helper):
    groups = []
    a = set(cover_helper.columns.values)
    seen_objects = set()
    seen_coverage = set()
    g = 0
    while len(a) > 0:
        c = a.pop()
        z = set()
        z.add(c)
        founded_coverage = set()
        founded_coverage.add(c)
        group_objects = set()

        while len(founded_coverage) > 0:
            c = founded_coverage.pop()

            objects = set(cover_helper[c].index[cover_helper[c]
                                                [cover_helper[c].index] == 1].values) - seen_objects
            seen_coverage.add(c)

            for ind in objects:
                row = cover_helper.loc[ind]
                founded_coverage.update(row.index[row[row.index] == 1])
            z.update(founded_coverage)
            seen_objects.update(objects)
            group_objects.update(objects)

            founded_coverage = founded_coverage - seen_coverage

        groups.append(ObjectGroup(g, z, group_objects))

        print(f"Group #{g} :")
        print("\tcoverage objects: ", z)
        print("\tcoverage objects count: ", len(z))

        print("\tobjects: ", group_objects)
        print("\tobjects count: ", len(group_objects))
        g += 1
        a = a - z
    return groups
