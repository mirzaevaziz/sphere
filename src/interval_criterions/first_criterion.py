from collections import namedtuple

import numpy as np
import pandas as pd

Result = namedtuple('Result', 'object_index distance value distances')


def find(class_count: int, non_class_count: int, class_value: int,
         distances: pd.DataFrame) -> Result:
    max_val = None
    prev_distance = None

    # to_class_dist = distances[distances[1] == object_set.class_value]
    # to_non_class_dist = distances[distances[1] != object_set.class_value]

    distances = distances.sort_values(by=0)

    denominator1 = class_count * (class_count - 1) + non_class_count * (non_class_count - 1)
    denominator2 = 2 * class_count * non_class_count
    u11 = 0
    u21 = 0
    result = None
    for obj_index in distances.index.values:
        curr_distance = distances[0][obj_index]
        if prev_distance is not None and prev_distance == curr_distance:
            continue
        prev_distance = curr_distance
        if distances[1][obj_index] == class_value:
            u11 = u11 + 1
        else:
            u21 = u21 + 1
        u12 = class_count - u11
        u22 = non_class_count - u21
        val1 = (u11 * (u11 - 1) + u21 * (u21 - 1) + u12 * (u12 - 1) + u22 * (u22 - 1)) / denominator1
        val2 = (u11 * (non_class_count - u21) + u21 * (class_count - u11) + u12 * (non_class_count - u22) + u22 * (
                class_count - u12)) / denominator2
        if max_val is None or max_val < val1 * val2:
            max_val = val1 * val2
            result = Result(obj_index, curr_distance, max_val, distances)
    return result


def find2(class_count: int, non_class_count: int, class_value: int, class_feature_index: int, objects: np.ndarray
          , distances: np.ndarray) -> Result:
    max_val = None
    obj_count = range(distances.shape[0])
    sort_counter = range(distances.shape[0] - 1)
    ind = list(obj_count)
    sorted_dist = np.empty((distances.shape[0], 2))
    for i in obj_count:
        sorted_dist[i][0] = i
        sorted_dist[i][1] = distances[i]

    sorted_dist = sorted_dist[sorted_dist[:, 1].argsort()]

    denominator1 = class_count * (class_count - 1) + non_class_count * (non_class_count - 1)
    denominator2 = 2 * class_count * non_class_count
    u11 = 0
    u21 = 0
    result = None
    for i in obj_count:
        obj_index = int(sorted_dist[i][0])
        if objects[obj_index][class_feature_index] == class_value:
            u11 = u11 + 1
        else:
            u21 = u21 + 1
        if sorted_dist.shape[0] > i + 1 and sorted_dist[i][1] == sorted_dist[i + 1][1]:
            continue
        u12 = class_count - u11
        u22 = non_class_count - u21
        val1 = (u11 * (u11 - 1) + u21 * (u21 - 1) + u12 * (u12 - 1) + u22 * (u22 - 1)) / denominator1
        val2 = (u11 * (non_class_count - u21) + u21 * (class_count - u11) + u12 * (non_class_count - u22) + u22 * (
                class_count - u12)) / denominator2
        if max_val is None or max_val < val1 * val2:
            max_val = val1 * val2
            result = Result(obj_index, sorted_dist[i][1], max_val, distances)
    return result
