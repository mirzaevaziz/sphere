from collections import namedtuple

import numba
import pandas as pd

Result = namedtuple('Result', 'object_index1 object_index2 value distances')


@numba.jit
def find(class_count: int, non_class_count: int, class_value: int,
         distances: pd.DataFrame) -> Result:
    max_val = None
    prev_distance = None

    # to_class_dist = distances[distances[1] == object_set.class_value]
    # to_non_class_dist = distances[distances[1] != object_set.class_value]

    distances = distances.sort_values(by=0)

    denominator1 = class_count * (class_count - 1) + non_class_count * (non_class_count - 1)
    denominator2 = 2 * class_count * non_class_count
    u11 = 0
    u21 = 0
    result = None
    for obj_index in distances.index.values:
        curr_distance = distances[0][obj_index]
        if prev_distance is not None and prev_distance == curr_distance:
            continue
        prev_distance = curr_distance
        if distances[1][obj_index] == class_value:
            u11 = u11 + 1
        else:
            u21 = u21 + 1
        u12 = class_count - u11
        u22 = non_class_count - u21
        val1 = (u11 * (u11 - 1) + u21 * (u21 - 1) + u12 * (u12 - 1) + u22 * (u22 - 1)) / denominator1
        val2 = (u11 * (non_class_count - u21) + u21 * (class_count - u11) + u12 * (non_class_count - u22) + u22 * (
                class_count - u12)) / denominator2
        if max_val is None or max_val < val1 * val2:
            max_val = val1 * val2
            result = Result(obj_index, curr_distance, max_val, distances)
    return result
